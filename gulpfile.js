'use strict';
var gulp = require('gulp');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');
var minify = require('gulp-clean-css');
var del = require('del');

var DEST = 'dest/';
var SRC = 'src/';

/**
*   压缩css
*/
gulp.task('uglifycss',function(){
    // 将你的默认的任务代码放在这
  return gulp.src(['*.css'])
            .pipe(gulp.dest(SRC+'css/'))
            .pipe(minify())
            .pipe(rename({suffix: '.min'}))
            .pipe(gulp.dest(DEST+'css/'));
});
/**
*   压缩js
*/
gulp.task('uglifyjs',function(){
    // 将你的默认的任务代码放在这
  return gulp.src(['*.js','!gulpfile.js'])
            .pipe(gulp.dest(SRC+'js/'))
            .pipe(uglify())
            .pipe(rename({suffix: '.min'}))
            .pipe(gulp.dest(DEST+'js/'));
});
/**
*  先清理
*/
gulp.task('clean',function(cb){
    return del([DEST,SRC],cb);

});
//默认任务
gulp.task('default', ['clean'],function(){
    console.log('执行任务');
    //执行其他任务
    gulp.start('uglifycss','uglifyjs');
});