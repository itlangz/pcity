# 中国行政省市区三级级联插件

-----

##文件目录说明
### dest目录是压缩之后的css和js
### src目录是css和js的源码

##使用介绍
1、在页面中引入js和css
```HTML
<!-- 公共的样式 -->
<link rel="stylesheet" type="text/css" href="./dest/css/pml.min.css">
<!-- 
    省市区的数据
    [
        {
            code : '110000',
            name : '北京市',
            value : '110000',
            subs : [
                    {
                        code : '110100',
                        name : '市辖区',
                        value : '110100',
                        subs : [
                            code : '110101',
                            name : '东城区',
                            value : '110101',
                        ]
                    }
                ]
        }
    ]
-->
<script type="text/javascript" src="./dest/js/region.min.js"></script>
<!-- 主要的JS -->
<script type="text/javascript" src="./dest/js/pml.min.js"></script>
```
2、在body中定义一个div
```HTML
<div class="main" data-value="420000,420300,420325">省市级联</div>
```
3、调用说明
```javascript
<script type="text/javascript">
 window.onload = function(){
        //data是必须的,end可以不用传
        var pmlArea = new $.PmlArea({data : region,end : function(val,txt){
            //val:value,txt:name
            //可自行处理结果
            console.log(val,txt);
        }});
        $('.main').click(function(e){
            //e 必须传
            pmlArea.show(e);
        })

    }
</script>
```
## API
  > * 若引用的jQuery等插件，请将此处的**$**更换为**pml**,即:
  
```javascript
    new $.PmlArea(options) <===> new pml.PmlArea(options);
```
> * options参数说明：
```javascript
    options = {
        data : [],          //省市区的数据 必传
        split : ',',        //得到数据的分隔符,默认',';效果：湖北省,十堰市,张湾区
        autoClose : true,   //是否自动关闭
        defaultVal ：[],    //默认值
        timeout ： 0,       //关闭的延迟的时间
        end : null,         //关闭后的回调
    }
```
> * show(event,[defaultVal],[end])方法参数说明：
> event : event对象 必传
    defaultVal:默认值，会覆盖options中的defaultVal
    end : function(){} 回调会覆盖options中的end
> * 设置默认值 
1)、在元素上设置属性data-value="420000,420300,420302",分隔符必须和options参数中保持一致,会覆盖options中的默认值
2)、通过show()方法的第二个参数,会覆盖data-value和options的默认值
> * 默认值的优先级: show() > data-value > options.defaultVal

